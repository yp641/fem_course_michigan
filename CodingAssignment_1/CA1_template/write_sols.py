import numpy as np


E = 1e11
A = 1e-4
f = 1e11
L = 0.1
g1 = 0.0
g2 = 0.001
h = 1e6

def prob1(z):
    res = (-1 / (6 * E)) * f * (z ** 3)
    res += (((g2 - g1) / L) + f * (L ** 2) / (6 * E)) * z
    res += g1

    return res


def prob2(z):
    res = (-1 / (6 * E)) * f * (z ** 3)
    res += (h / (E * A) + f * (L ** 2) / (2 * E)) * z
    res += g1

    return res

print('Problem 1')
for val in [0.0, 0.01, 0.09, 0.1]:
    print(f'u({val:.2f})  =  {prob1(val)}')

print('Problem 2')
for val in [0.0, 0.01, 0.09, 0.1]:
    print(f'u({val:.2f})  =  {prob2(val)}')
